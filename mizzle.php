<?php
/**
* Файл для проверки класса DB2MizzleXML
*/

include "DB2MizzleXML.php";
$host = 'localhost'; 
$user = 'mizzle';
$password = 'mizzle';
$dbname = 'mizzle';

//Соединение с базой данных
try {
    $db = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec("SET NAMES utf8");
}
catch(PDOException $e) {
    echo $e->getMessage();
}
$mizzle = new DB2MizzleXML();
$mizzle->addCurrency(DB2MizzleXML::CURRENCY_USD, 34.12);

//Запрос на выборку всех товаров
$query ="SELECT id, name, short, price_mid, available FROM mz_product LIMIT 10";
$result = $db->query($query);

//Добавление позиций
while($row = $result->fetch())
{
    $mizzle->createPositionByUrl('http://mizzle.ru/product/'.$row["id"].'/'.$row["short"], $row['price_mid'], 'http://mizzle.ru/position/buy/'.$row["id"]);
}
/*
//Запрос на выборку всех товаров
$query = "SELECT id, name, short, price_mid, available FROM mz_product LIMIT 10 OFFSET 10";
$result = $db->query($query);

//Добавление позиций

while($row = $result->fetch())
{
    $mizzle->createPositionByID($row["id"], $row["price_mid"], 'http://mizzle.ru/position/buy/'.$row["id"]);
}

//Запрос на выборку всех товаров
$query = "SELECT id, name, short, price_mid, available FROM mz_product LIMIT 10 OFFSET 20";
$result = $db->query($query);

//Добавление позиций

while($row = $result->fetch())
{
    $mizzle->createPositionByName($row["name"], $row["price_mid"], 'http://mizzle.ru/position/buy/'.$row["id"]);
}

//Запрос на выборку всех товаров
$query = "SELECT id, name, short, price_mid, available FROM mz_product LIMIT 10 OFFSET 30";
$result = $db->query($query);

//Добавление позиций 
while($row = $result->fetch())
{
    $mizzle->createPositionByHash($row["short"], $row["price_mid"], 'http://mizzle.ru/position/buy/'.$row["id"]);
}
*/
//Сохранение в файл
$mizzle->save('pricelist.xml');

?>