<?php
/**
 * файл класса MizzleXML
 * 
 * @author Muzaffar Artikov <artikovmuzaffar@gmail.com>
 * 
 * MizzleXML может быть использовано таким образом
 * <pre>
 *      $mizzle = new MizzleXML();
 *      $mizzle->createPositionByUrl('http://mizzle.ru/product/6/wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy','USD',true);
 *      $mizzle->save('pricelist.xml');
 * </pre>
 *      ##$mizzle->addCurrency('USD', 32.34);
 *      ##$mizzle->createPositionByID(6, 19.95, 'http://goldshop8934.ru/wow-gold-buy');
 *      ##$mizzle->createPositionByHash('wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
 *      ##$mizzle->createPositionByName('1к золота: Азурегос - Альянс', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
 */
class DB2MizzleXML{
    
    //константные значения валют
    const CURRENCY_RUR = 'RUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_UAH = 'UAH';
    const CURRENCY_EUR = 'EUR';
    /**
     *
     * @var DomDocument 
     */
    private $dom;
    
    /**
     *
     * @var array 
     */
    private $currencies = array(self::CURRENCY_RUR=>1.0);
    
    /**
     *
     * @var DomElement 
     */
    private $positions;

    /**
     * Constructor
     * Создаем заголовок для xml документа
     * Создаем элемент positions
     */
    public function __construct(){
        $this->dom = new DOMDocument('1.0', 'utf-8');
        $this->positions = $this->dom->createElement('positions');
    }
    /**
     * Добавляет новую валюту и его текущий курс относительно рубля.
     * @param string $currency код валюты
     * @param float $rate курс относительно рубля
     */       
    public function addCurrency($currency, $rate){
        $this->currencies[$currency] = $rate;
    }
    /**
     * Создание xml элемента позиции по url товара на миззл
     * @param string $mizzleUrl url товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты, по умолчанию RUR (российский рубль)
     * @param boolean $available доступность товара
     */
    public function createPositionByUrl($mizzleUrl, $price, $buyUrl, $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
	    $url  = $this->dom->createElement('url',$mizzleUrl);
	    $product->appendChild($url);
        $_price = $this->dom->createElement('price',$price);
        $_available = $this->dom->createElement('available',$available);
        $buy_url = $this->dom->createElement('buyurl',$buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        /* Более валюта не задается
         * if($currencyId != self::CURRENCY_RUR){
            $currency_id = $this->dom->createElement('currencyid',$currencyId);
            $position->appendChild($currency_id);
        }*/
        $position->appendChild($_available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
    /**
     * Создание xml элемента позиции по ID товара на миззл
     * @param string $mizzleID id товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты, по умолчанию RUR (российский рубль)
     * @param boolean $available доступность товара
     */
	/*
    public function createPositionByID($mizzleID, $price, $buyUrl, $currencyId = self::CURRENCY_RUR, $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
	    $id  = $this->dom->createElement('id', $mizzleID);
	    $product->appendChild($id);
        $_price = $this->dom->createElement('price',$price);
        $currency_id = $this->dom->createElement('currencyid', $currencyId);
        $_available = $this->dom->createElement('available', $available);
        $buy_url = $this->dom->createElement('buyurl', $buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        if($currencyId != self::CURRENCY_RUR){            
            $currency_id = $this->dom->createElement('currencyid', $currencyId);
            $position->appendChild($currency_id);
        } 
        $position->appendChild($_available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
	*/
    /**
     * Создание xml элемента позиции по hash товара на миззл
     * @param string $mizzleHash идентификационный хеш товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты, по умолчанию RUR (российский рубль)
     * @param boolean $available доступность товара
     */
	/*
    public function createPositionByHash($mizzleHash, $price, $buyUrl, $currencyId = self::CURRENCY_RUR, $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
            $hash  = $this->dom->createElement('hash', $mizzleHash);
            $product->appendChild($hash);
        $_price = $this->dom->createElement('price', $price);
        $currency_id = $this->dom->createElement('currencyid', $currencyId);
        $_available = $this->dom->createElement('available', $available);
        $buy_url = $this->dom->createElement('buyurl', $buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        if($currencyId != self::CURRENCY_RUR){            
            $currency_id = $this->dom->createElement('currencyid', $currencyId);
            $position->appendChild($currency_id);
        } 
        $position->appendChild($_available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
	*/
    /**
     * Создание xml элемента позиции по name товара на миззл
     * @param string $mizzleName название товара на миззл
     * @param float $price цена товара
     * @param string $buyUrl url покупки
     * @param string $currencyId код валюты, по умолчанию RUR (российский рубль)
     * @param boolean $available доступность товара
     */
	/*
    public function createPositionByName($mizzleName, $price, $buyUrl, $currencyId = self::CURRENCY_RUR, $available = true){
        
        $position = $this->dom->createElement('position');
        $product  = $this->dom->createElement('product');
	    $name  = $this->dom->createElement('name', $mizzleName);
	    $product->appendChild($name);
        $_price = $this->dom->createElement('price', $price);
        $currency_id = $this->dom->createElement('currencyid', $currencyId);
        $_available = $this->dom->createElement('available', $available);
        $buy_url = $this->dom->createElement('buyurl', $buyUrl);
        
        $position->appendChild($product);
        $position->appendChild($_price);
        if($currencyId != self::CURRENCY_RUR){            
            $currency_id = $this->dom->createElement('currencyid', $currencyId);
            $position->appendChild($currency_id);
        } 
        $position->appendChild($_available);
        $position->appendChild($buy_url);
        
        $this->positions->appendChild($position);
    }
	*/
    /**
     * Подготовка xml документа в формате миззл
     */
    private function prepare(){
        $mizzle = $this->dom->createElement('mizzle');
        $mizzle->setAttribute('date', date('Y-m-d H:i'));
        //Если валюты больше одного, есть смысл создать элемент <currencies>
		/* Ненадо!
		 if(count($this->currencies) > 1){
            $currencies = $this->dom->createElement('currencies');
            foreach($this->currencies as $currency=>$rate){
                $curr = $this->dom->createElement('currency');
                $curr->setAttribute('id', $currency);
                $curr->setAttribute('rate', $rate);
                $currencies->appendChild($curr);
            }
            $mizzle->appendChild($currencies);
        }
		*/
        $mizzle->appendChild($this->positions);   
        $this->dom->appendChild($mizzle);
    }
    /**
     * Сохранение xml документа в файл
     * @param string $filename имя файла
     */
    public function save($filename){
        $this->prepare();
        $this->dom->save($filename);
    }
}
?>