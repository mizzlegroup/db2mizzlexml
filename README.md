**DB2MizzleXML** – PHP класс, который позволяет создать xml-файл с позициями товаров вашего магазина для Mizzle.


После подключения файла класса, нужно создать объект класса DB2MizzleXML. Например:
```php
$mizzle = new DB2MizzleXML();
```


Если товары вашего магазина продаются в нескольких валютах, то следует добавить валюты с помощью функции `addCurrency`.

Функция addCurrency принимает 2 параметра:

1. Идентификатор валюты.

2. Текущий курс относительно российского рубля.

Например:
```php
$mizzle->addCurrency(DB2MizzleXML::CURRENCY_USD, 34.12);
```


Стоить отметить, что в классе уже есть  идентификаторы для 4 валют. Если не указать валюты, по умолчанию берется российский рубль для позиции:

1. `CURRENCY_RUR` – для определения российского рубля, курс равен 1.0.

2. `CURRENCY_USD` - для определения доллара США.

3. `CURRENCY_UAH` - для определения украинской гривны.

4. `CURRENCY_EUR` - для определения евро.


Теперь можно добавить позиции. Для создания позиции можно использовать следующие 4 функции. Для всех этих функций обязательными является 3 параметра.



#### createPositionByUrl()
Cоздает позицию по url адресу продукта. Параметры:

1. URL-адрес продукта на mizzle.ru.

2. Цена продукта.

3. URL-адрес покупки продукта.

4. Тип валюты (указывается, если был добавлен с помощью addCurrency).

5. Доступность товара (по умолчанию доступен, если не доступен следует указать false).

Пример использования:
```php
$mizzle->createPositionByUrl('http://mizzle.ru/product/6/wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy',DB2MizzleXML::CURRENCY_USD,true);
```



#### createPositionByID()
Создает позицию по id товара. Параметры:

1. ID продукта на mizzle.ru.

2. Цена продукта.

3. URL-адрес покупки продукта.

4. Тип валюты (указывается, если был добавлен с помощью addCurrency).

5. Доступность товара (по умолчанию доступен, если не доступен следует указать false).

Пример использования:
```php
$mizzle->createPositionByID(6, 19.95, 'http://goldshop8934.ru/wow-gold-buy');
```



#### createPositionByHash()
Создает позицию по хеш идентификатору продукта. Хеш идентификаторы продуктов можете посмотреть по классификатору. Параметры:

1. Хэш-идентификатор продукта на mizzle.ru.

2. Цена продукта.

3. URL-адрес покупки продукта.

4. Тип валюты (указывается, если был добавлен с помощью addCurrency).

5. Доступность товара (по умолчанию доступен, если не доступен следует указать false).

Пример использования:
```php
$mizzle->createPositionByHash('wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
```



#### createPositionByName()
Cоздает позицию по названию продукта. Названия продуктов можете посмотреть по классификатору. Параметры:

1. Название продукта на mizzle.ru.

2. Цена продукта.

3. URL-адрес покупки продукта.

4. Тип валюты (указывается, если был добавлен с помощью addCurrency).

5. Доступность товара (по умолчанию доступен, если не доступен следует указать false).


Пример использования:
```php
$mizzle->createPositionByName('1к золота: Азурегос - Альянс', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
```




После добавления всех позиций, можно сохранить файл, используя функцию `save()`:

```php
$mizzle->save(‘path/to/pricelist.xml’);
```




Обобщенный код можно представить следующим образом.

```php
$mizzle = new DB2MizzleXML();
$mizzle->addCurrency(DB2MizzleXML::CURRENCY_USD, 34.12);
$mizzle->createPositionByUrl('http://mizzle.ru/product/6/wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy', DB2MizzleXML::CURRENCY_USD, true);
$mizzle->createPositionByID(6, 19.95, 'http://goldshop8934.ru/wow-gold-buy');
$mizzle->createPositionByHash('wow-gold-azuregos-alliance', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
$mizzle->createPositionByName('1к золота: Азурегос - Альянс', 19.95, 'http://goldshop8934.ru/wow-gold-buy');
$mizzle->save('pricelist.xml');
```
